#!/usr/bin/env node

'use strict';
var program = require('commander'); // CLI arguments parser and runner
var q = require('q');               // Promises

// Internal modules
var metadata = require('../package.json');                  // Package metadata
var preferences = require('./components/preferences');      // Application preferences  
var logger = require('./components/logger');                // Debug-mode logger
var dataService = require('./components/data-service');     // Data provider 
var ui = require('./components/ui');                        // UI renderer

// Load preferences
preferences.load();

// Parse CLI options  
program
    .version(metadata.version)
    .option('-d, --debug', 'Display debug information')
    .parse(process.argv);
       
// Pass options to components
logger.options = program; 
ui.options = program;
dataService.options = program;
    
// Define commands
program
    .command('stop <number>')
    .description('Real-time departures at the specified stop')
    .action(loadStop);

program
    .command('find <text>')
    .description('Stop lookup by name and real-time departures')
    .action(function(text) {
        dataService.findStops(text)
            .then(ui.selectStop)
            .then(loadStop)
            .catch(ui.noStopsFound);
    });

program
    .command('line <name>')
    .description('Line details and stops')
    .action(function(line) {        
        dataService.getLine(line)
            .then(ui.line)
            .then(ui.displayLine)
            .then(ui.line)
            .catch(ui.noDataFound);
    });

program
    .command('help')
    .description('Displays help')
    .action(function() {
        program.help();
    });

program
    .command('*')
    .description('Short-hand syntax for quick arrivals at the stop')
    .action(findStop);
    
program
    .command('preferences [group] [command] [value]')
    .description('Displays or modifies application preferences \npreferences list \npreferences stop add <number> \npreferences stop remove <number>')
    .action(changePreferences);


// Parse arguments and execute commands
program.parse(process.argv);    
if (process.argv.length <= 2) {
    // if default stops specified in preferences, load real-time information
    // otherwise just help, there's nothing dumber than a dumb program that says noting
    loadPreferredStops()
        .catch(function() { 
            program.help(); 
        });            
}

// Loads and displays preferred stops.
// Returns a promise which fails if there are no preferred stops to display.
function loadPreferredStops() {
    return q.Promise(function(resolve, reject) {
        if (!preferences || !preferences.stops || !preferences.stops.length) {
            reject();
        }
        else {
            // create a series of promises
            var series = preferences.stops.reduce(function (promise, stopId) {
                return promise.then(function() {
                    return loadStop(stopId).then(ui.line());
                })      
            }, q.resolve());
            // run sequentially
            series.then(function() {                 
                resolve();
            });
        }
    });  
}

// Loads and displays real-time departures at the specified stop
// Returns a promise.
function loadStop(stopId) {
    return dataService.getStop(stopId)
        .then(ui.line)
        .then(ui.displayStop)
        .then(dataService.getDepartures)
        .then(ui.line)
        .then(ui.displayDepartures)
        .then(ui.line)
        .catch(ui.noDataFound);
}

// Loads and displays real-time departures at the specified stop.
// If stop name is specified, first determines the stop id.
// Returns a promise.
function findStop(stopIdOrName) {
    if (isNaN(parseInt(stopIdOrName)))
        return dataService.findStops(stopIdOrName)
            .then(ui.selectStop)
            .then(loadStop)
            .catch(ui.noStopsFound);
    else
        return loadStop(stopIdOrName);
}

// Preferences handler
function changePreferences(group, command, value) {
    switch (group) {
        // list preferences
        case "list": 
            ui.line();
            ui.displayPreferences();
            ui.line();

        // add/remove preferred stops
        case "stop":
            switch (command) {
                case "add": 
                    if (preferences.addStop(value))
                        ui.preferencesUpdated();
                    break;
                case "remove": 
                    if (preferences.removeStop(value))
                        ui.preferencesUpdated();
                    break;
            }
            break;
    }
}

