var stops = null;
var q = require('q');
var fetch = require('fetch');
var fs = require('fs');
var path = require('path');
var csv = require('csv-parser');
var format = require('stringformat');
format.extendString();
var logger = require('./logger');

// DUBLIN BUS DATA SERVICE
exports.options = {};

// Returns true if data set contains an error
exports.isError = function(data) {
    return data && data.errorCode && data.errorCode != "0";
}

// Returns true if data set contains results
exports.hasResults = function(data) {
    return data && data.numberofresults > 0 && data.results;
}

// Returns current week day, with Monday equal to 1 and Sunday equal to 7
exports.today = function() {
    var day = (new Date()).getDay();
    return day ? day : 7;
}

// Retrieves information about the specified stop
exports.getStop = function(stopId) {
    if (stopId && stopId.toString().trim() != '') {
        var url = endpoints.stop.format(stopId);
        return getData(url, messages.retrievingStop);
    }
    else
        return q.reject(stopId);
};

// Retrieves information about departures at the specified stop
exports.getDepartures = function(stopId) {
    if (stopId && stopId.toString().trim() != '') {
        var url = endpoints.departures.format(stopId);            
        return getData(url, messages.retrievingDepartures);
    }
    else
        return q.reject(stopId);
};

// Retrieves information about the specified route
exports.getRoute = function(routeId, operator) {    
    if (routeId && routeId.toString().trim() != '') {
        var url = endpoints.route.format(routeId, operator ? operator : 'bac');
        return getData(url, messages.retrievingRoute);
    }
    else
        return q.reject(routeId);
}

// Finds stops matching the specified words
exports.findStops = function(text) {
    if (text && text.toString().trim() != '') {
        return loadStops().then(function(stops) {
            var words = text.toString().trim().toLowerCase().split(" ");
            var found = stops.filter(function(stop) {            
                return words.every(function(word) {
                    return stop.name.toLowerCase().indexOf(word) > -1;
                });
            });
            return found;
        });
    }
    else
        return q.reject(text);
} 

// Retrieves weekly timetables for the specified stop and route  
exports.getTimetables = function(stopId, routeId) {    
    if (routeId && routeId.toString().trim() != '' && stopId && stopId.toString().trim() != '') {
        var url = endpoints.timetableweek.format(stopId, routeId);
        return getData(url, messages.retrievingTimetable);
    }
    else
        return q.reject(stopId);
}

// Retrieves timetable for the specified stop, route and day 
exports.getTimetable = function(stopId, routeId, dayOfWeek) {    
    var url = endpoints.timetableday.format(stopId, routeId, dayOfWeek);
    return getData(url, messages.retrievingTimetable);
}

// Extracts timetable matrices from the specified week timetables dataset
exports.createTimetableMatrices = function(data) {
    if (data) {
        var timetables = {
            days: {
                Monday: null,  
                Tuesday: null,
                Wednesday: null,
                Thursday: null,
                Friday: null,
                Saturday: null,
                Sunday: null
            },
            
            destinations: {  
            },
            
            defaultDestination: null
        };
        
        if (exports.hasResults(data)) {       
            var index = 1;
            for (var dayOfWeek in timetables.days)
                timetables.days[dayOfWeek] = { 
                    index: index++, 
                    departures: [] 
                };

            data.results.forEach(function(item) {
                // collect destinations
                if (!timetables.destinations[item.destination])
                    timetables.destinations[item.destination] = { 
                        name: item.destination,
                        count: 0
                    };
                // assign departures to appropriate week days
                var inRange = false;      
                for (var dayOfWeek in timetables.days) {
                    if (dayOfWeek == item.startdayofweek)
                        inRange = true;
                    if (inRange) {
                        item.departures.forEach(function(departure) {
                            // count destinations
                            timetables.destinations[item.destination].count++;
                            // add departure to day's timetable
                            timetables.days[dayOfWeek].departures.push({
                               time: departure,
                               destination: item.destination 
                            });
                        });
                    }
                    if (dayOfWeek == item.enddayofweek)
                        inRange = false;
                }      
            });
            
            // Find most popular destination, to be assumed default for the timetable.
            // Departures to non-standard destinations will be indicated.
            var max = 0;
            for (var name in timetables.destinations) {
                var destination = timetables.destinations[name]; 
                if (destination.count > max) {
                    timetables.defaultDestination = destination;
                    max = timetables.defaultDestination.count;
                }
            }
            timetables.defaultDestination.isDefault = true;
            timetables.defaultDestinationName = timetables.defaultDestination.name;
            
            // sort timetables
            for (var dayOfWeek in timetables.days) {
                var day = timetables.days[dayOfWeek];
                day.name = dayOfWeek;
                day.destinations = timetables.destinations;
                day.defaultDestination = timetables.defaultDestination;
                day.defaultDestinationName = timetables.defaultDestination.name;
                day.departures.sort(function(a, b) {
                    return a.time.localeCompare(b.time);
                });            
                // eliminate duplicates
                var departures = [];
                var previous = null;
                for (var i=0; i<day.departures.length; i++) {
                    if (day.departures[i].time != previous) {
                        departures.push(day.departures[i]);
                        previous = day.departures[i].time;
                    }
                } 
                day.departures = departures;
            }
        }
        return q.when(timetables);
    }
    else
        q.reject(data);
}

// Finds day timetable from the specified week matrices
exports.findDayTimetable = function(stopId, routeId, dayIndex, timetables) {
    var timetable = null;
    for (var dayOfWeek in timetables.days) {
        var day = timetables.days[dayOfWeek];
        day.stopId = stopId;
        day.routeId = routeId;
        if (day.index.toString() == dayIndex.toString()) {
            timetable = day;
            break;
        }
    }
    return q.when(timetable);
}


// Service endpoints
var endpoints = {
    stop: 'https://data.dublinked.ie/cgi-bin/rtpi/busstopinformation?format=json&stopid={0}',
    route: 'https://data.dublinked.ie/cgi-bin/rtpi/routeinformation?format=json&routeid={0}&operator={1}',
    departures: 'https://data.dublinked.ie/cgi-bin/rtpi/realtimebusinformation?format=json&stopid={0}', 
    timetableweek: 'https://data.dublinked.ie/cgi-bin/rtpi/timetableinformation?format=json&type=week&stopid={0}&routeid={1}',
    timetableday: 'https://data.dublinked.ie/cgi-bin/rtpi/timetableinformation?format=json&type=day&stopid={0}&routeid={1}&day={2}'
};

// Status messages
var messages = {
    retrievingStop: 'Retrieving information about the stop ...',
    retrievingRoute: 'Retrieving information about the route ...',
    retrievingDepartures: 'Retrieving real-time departures ...',
    retrievingTimetable: 'Retrieving timetable ...',
    noStopsFound: 'No stops found',
    error: 'There was an error whzile handling your request.',
    noResults: "No results returned"
};

// Retrieves data from the specified endpoint
function getData(url, message) {
    logger.log(message);
    logger.log('URL: {0}'.format(url));
    logger.log();
    return q.Promise(function(resolve, reject) {
        fetch.fetchUrl(url, function(error, meta, body) {
            if (error) {
                logger.error(error.toString());
                return reject(error);
            }
            else {
                var text = body.toString();
                var data = JSON.parse(text);
                logger.log(text);
                if (exports.isError(data))
                    return reject(data);
                else if (!exports.hasResults(data))
                    return reject(data);
                else
                    return resolve(data);
            }
        });
    }); 
}

// Loads stops metadata
function loadStops() {
    if (stops)
        return q.when(stops); 
    return q.Promise(function(resolve, reject) {            
        var fileName = __dirname + '/../data/stops.txt';
        fs.createReadStream(fileName)
        .pipe(csv())
        .on('data', function(stop) {
            if (stops==null)
                stops = [];
            stops.push({
                code: stop["stop_code"],
                name: stop["stop_name"],
                latitude: parseFloat(stop["stop_lat"]),
                longitude: parseFloat(stop["stop_lon"])
            });
        })  
        .on('end', function() {
            resolve(stops);
        });  
    });
}

