#Dublin LUAS CLI

##Introduction
Command-line client for Dublin LUAS, younger brother of [Dublin Bus application](https://www.npmjs.com/package/dublin-bus).
It allows access to real-time information about tram departures along Dublin LUAS lines. Just like Dublin Bus application,
it works also on your Android phone!

If you love it, why not rewarding me by a €2 PayPal donation? 
Every two and a half donation buys me a pint of [the good black stuff](https://en.wikipedia.org/wiki/Guinness) :-) 

* [PayPal.me €2 at https://www.paypal.me](https://www.paypal.me/tomaszwaraksa/2)
* [PayPal transfer at https://www.paypal.com using my e-mail address *tomasz@waraksa.net*](https://www.paypal.com/myaccount/transfer/send) 

Your name will be added to the list of donors and you'll be famous forever, as Internet never forgets! 

Merci beaucoup!

Tomasz 
